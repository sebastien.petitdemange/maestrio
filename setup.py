from distutils.core import setup

setup(name="maestrio", version="0.1",
      description="Maestrio controller",
      author="bliss@esrf.fr",
      package_dir={"maestrio": "maestrio"},
      packages=["maestrio"])
