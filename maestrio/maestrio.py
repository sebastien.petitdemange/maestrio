# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.
import re
import enum
import functools
import weakref
import struct
import numpy
from bliss.comm.tcp import Command
from bliss.common.utils import autocomplete_property
from bliss import global_map

from .acquisition.maestrio import MaestrioCounterController

def debug(funct):
    def f(*args,**kwargs):
        print (args,kwargs)
        values = funct(*args,**kwargs)
        print('return', values)
        return values
    return f

def _get_simple_property(command_name, doc_sring):
    def get(self):
        return self.putget("?%s" % command_name)

    def set(self, value):
        return self.putget("%s %s" % (command_name, value))

    return property(get, set, doc=doc_sring)


def _simple_cmd(command_name, doc_sring):
    def exec_cmd(self):
        return self.putget(command_name)

    return property(exec_cmd, doc=doc_sring)

CHANNEL_MODE = enum.Enum("CHANNEL_MODE","QUADRATURE PULSES "
                         "SSI BISS HSSL ENDAT ADC")
INPUT_MODE = enum.Enum("INPUT_MODE","TTL TTL50  NIM VOLT")
OUTPUT_MODE = enum.Enum("OUTPUT_MODE","OFF NIM TTL FREQ VOLT")
SMART_ACC_MODE = enum.Enum("SMART_ACC_MODE","UNDEF COUNT_UP COUNT_UPDOWN INTEGR READ SAMPLE TRACK")

def _activate_switch(func):
    @functools.wraps(func)
    def f(self,*args,**kwargs):
        if self._switch is not None:
            self._switch.set(self._switch_name)
        return func(self,*args,**kwargs)
    return f

def lazy_init(func):
    @functools.wraps(func)
    def f(self, *args, **kwargs):
        self._init()
        return func(self, *args, **kwargs)

    return f

# Base class for Inputs Outputs and Channels.
class _BaseInputOutput:
    def __init__(self,maestrio,channel_id,switch=None, switch_name=None):
        self._maestrio = weakref.ref(maestrio)
        self._channel_id = channel_id
        if switch is not None:
            # check if has the good interface
            if switch_name is None:
                raise RuntimeError(
                    f"maestrio: {self.__class__.__name__} ({channel_id}) with external "
                    "switch maestrio have a switch_name defined"
                )
            if not hasattr(switch, "set"):
                raise RuntimeError(
                    f"maestrio: {self.__class__.__name__} ({channel_id}), "
                    "switch object doesn't have a set method"
                )
            self._switch = switch
            self._switch_name = switch_name
        else:
            self._switch = None
    @property
    @_activate_switch
    def channel_id(self):
        return self._channel_id

    @property
    def switch(self):
        return self._switch

class Maestrio:
    BINARY_SIGNATURE = 0xA5A50000
    BINARY_NOCHECKSUM = 0x00000010
    PROG_STATE = enum.Enum("PROGRAM_STATE","NOPROG BAD UNLOAD STOP IDLE RUN BREAK")
    CHANNEL_MODE = CHANNEL_MODE
    INPUT_MODE = INPUT_MODE
    OUTPUT_MODE = OUTPUT_MODE
    SMART_ACC_MODE = SMART_ACC_MODE

    class channel(_BaseInputOutput):
        def __init__(self, maestrio, channel_id, mode=None, switch=None, switch_name=None):
            super().__init__(maestrio,channel_id,switch=switch,switch_name=switch_name)
            self._mode = None
            if mode is not None:
                if isinstance(mode, str):
                    MODE = type.upper()
                    try:
                        mode = CHANNEL_MODE[MODE]
                    except KeyError:
                        raise RuntimeError("maestrio: mode (%s) is not known" % type)
                    self._mode = mode
                else:
                    self._mode = mode

        @property
        def mode(self):
            if self._mode is None:
                self._read_config() # init mode
            return self._mode


        @property
        @_activate_switch
        def value(self):
            maestrio = self._maestrio()
            string_value = maestrio.putget("?CHVAL C%d" % self._channel_id)
            return self._convert(string_value)

        @value.setter
        @_activate_switch
        def value(self, val):
            maestrio = self._maestrio()
            maestrio.putget("CHVAL C%d POS %s" % (self._channel_id, val))

        @property
        @_activate_switch
        def raw_value(self):
            maestrio = self._maestrio()
            string_value = maestrio.putget("?CHVAL C%d RAWPOS" % self._channel_id)
            return self._convert(string_value)

        @property
        @_activate_switch
        def offset(self):
            maestrio = self._maestrio()
            string_value = maestrio.putget("?CHVAL C%d OFFSET" % self._channel_id)
            return self._convert(string_value)

        @offset.setter
        @_activate_switch
        def offset(self,value):
            maestrio = self._maestrio()
            maestrio.putget("?CHVAL C%d OFFSET %s" % (self._channel_id,value))

        def _convert(self, string_value):
            """Return channel value, converted according to the configured mode.
            """
            return string_value # TODO????

        def _read_config(self):
            """Read configuration of the current channel from MAESTRIO board to
            determine the usage mode of the channel.
            """
            if self._mode is None:
                maestrio = self._maestrio()
                string_config = maestrio.putget("?CHCFG C%d" % self._channel_id)
                split_config = string_config.split()
                self._mode = CHANNEL_MODE[split_config[1]]

        def __info__(self):
            maestrio = self._maestrio()
            info = maestrio.putget("?CHCFG C%d" % self._channel_id)
            info += f"\t Value: {self.value}"
            return info

    class input(_BaseInputOutput):
        def __init__(self, maestrio, channel_id, mode=None, switch=None, switch_name=None):
            super().__init__(maestrio,channel_id,switch=switch,switch_name=switch_name)
            self._mode = None

        @property
        def mode(self):
            if self._mode is None:
                self._read_config() # init mode
            return self._mode

        @property
        def value(self):
            maestrio = self._maestrio()
            string_value = maestrio.putget("?INVAL I%d" % self._channel_id)
            return self._convert(string_value)
        
        def _convert(self,string_value):
            return string_value

        def _read_config(self):
            """Read configuration of the current init from MAESTRIO board to
            determine the usage mode of the input.
            """
            if self._mode is None:
                maestrio = self._maestrio()
                string_config = maestrio.putget("?INCFG I%d" % self._channel_id)
                split_config = string_config.split()
                self._mode = INPUT_MODE[split_config[1]]
                
        def __info__(self):
            maestrio = self._maestrio()
            info = maestrio.putget("?INCFG I%d" % self._channel_id)
            info += f"\t Value: {self.value}"
            return info

    class output(_BaseInputOutput):
        def __init__(self, maestrio, channel_id, mode=None, switch=None, switch_name=None):
            super().__init__(maestrio,channel_id,switch=switch,switch_name=switch_name)
            self._mode = None

        @property
        def mode(self):
            if self._mode is None:
                self._read_config() # init mode
            return self._mode

        @property
        def value(self):
            maestrio = self._maestrio()
            string_value = maestrio.putget("?OUTVAL O%d" % self._channel_id)
            return self._convert(string_value)
        
        @value.setter
        def value(self,value):
            maestrio = self._maestrio()
            maestrio.putget("OUTVAL O%d %s" % (self._channel_id,value))
            
        
        def _convert(self,string_value):
            return string_value

        def _read_config(self):
            """Read configuration of the current init from MAESTRIO board to
            determine the usage mode of the output.
            """
            if self._mode is None:
                maestrio = self._maestrio()
                string_config = maestrio.putget("?OUTCFG I%d" % self._channel_id)
                split_config = string_config.split()
                self._mode = OUTPUT_MODE[split_config[1]]
                
        def __info__(self):
            maestrio = self._maestrio()
            info = maestrio.putget("?OUTCFG O%d" % self._channel_id)
            info += f"\t Value: {self.value}"
            return info


        
    APPNAME =  _simple_cmd("?APPNAME", "Return application name")
    VERSION =  _simple_cmd("?VERSION", "Return application version")
    HELP = _simple_cmd("?HELP", "Return list of commands")
    HELP_ALL = _simple_cmd("?HELP ALL", "Return all list of commands")


    def __init__(self,name,config_tree):
        self._name = name
        hostname = config_tree.get("host")
        self._cnx = Command(hostname,5000,eol="\n")
        global_map.register(self,children_list=[self._cnx])

        self._counters_container = self._get_sampling_counter_controller(config_tree)

    @property
    def name(self):
        return self._name
    
    def _init(self):
        pass

    def _get_sampling_counter_controller(self,config_tree):
        """
        This method can be defined in sub-classes and return a specific
        Counter controller
        """
        return MaestrioCounterController(self,config_tree)

    #@debug
    def putget(self,cmd,data=None):
        """ Raw connection to the Maestrio card.

        cmd -- the message you want to send
        data -- binnary or ascii data
        """
        _check_reply = re.compile(r"^[#?]")
        reply_flag = _check_reply.match(cmd)
        cmd_raw = cmd.encode()
        cmd_name = cmd_raw.split()[0].strip(b' #').upper()
        if data is not None:
            #check if string; i.e program
            if isinstance(data,str) or isinstance(data,bytes):
                if isinstance(data,str):
                    raw_data = data.encode()
                else:
                    raw_data = data
                    
                header = struct.pack("<III",
                                     self.BINARY_SIGNATURE|self.BINARY_NOCHECKSUM|0x1,
                                     len(raw_data),
                                     0x0) # no checksum
                full_cmd = b"%s\n%s%s" % (cmd_raw,header,raw_data)
                transaction = self._cnx._write(full_cmd)
            elif isinstance(data,numpy.ndarray):
                if data.dtype in [numpy.uint8,numpy.int8,
                                  numpy.uint16,numpy.int16,
                                  numpy.uint32,numpy.int32,
                                  numpy.uint64,numpy.int64]:
                    raw_data = data.tostring()
                    header = struct.pack("<III",
                                         self.BINARY_SIGNATURE|self.BINARY_NOCHECKSUM|data.dtype.itemsize,
                                         int(len(raw_data)/data.dtype.itemsize),
                                         0x0) # no checksum
                    full_cmd = b"%s\n%s%s" % (cmd_raw,header,raw_data)
                    transaction = self._cnx._write(full_cmd)
                else:
                    raise RuntimeError(f"Numpy datatype {data.dtype} not yet managed")
            else:
                raise RuntimeError("Not implemented yet ;)")
        else:
            transaction = self._cnx._write(cmd_raw + b'\n')
        with self._cnx.Transaction(self._cnx,transaction):
            if reply_flag:
                msg = self._cnx._readline(transaction=transaction,clear_transaction=False)
                if not msg.startswith(cmd_name):
                    print(msg)
                    raise RuntimeError(f"Unknown error, send {cmd}, reply:",msg.decode())
                if msg.find(b'ERROR') > -1:
                    #patch to workaround protocol issue in maestrio
                    #in Error, maestrio send two '\n' instead of one
                    if (not transaction.empty() and transaction.peek()[0:1] == b'\n'):
                        data = transaction.get()[1:]
                        if len(data):
                            transaction.put(data)
                    raise RuntimeError(msg.decode())
                elif msg.find(b'$') > -1 : # multi line
                    msg = self._cnx._readline(transaction=transaction,
                                              clear_transaction=False,
                                              eol=b'$\n')
                    return msg.decode()
                elif msg.find(b'?*') > -1: # binary reply
                    header_size = struct.calcsize('<III')
                    header = self._cnx._read(transaction,size=header_size,clear_transaction=False)
                    magic_n_type,size,check_sum = struct.unpack('<III',header)
                    magic = magic_n_type & 0xFFFF0000
                    assert magic == self.BINARY_SIGNATURE
                    raw_type = magic_n_type & 0xF
                    numpy_type = {1:numpy.uint8,2:numpy.uint16,4:numpy.uint32}.get(raw_type)
                    data = self._cnx._read(transaction,
                                           size=size*numpy_type().dtype.itemsize,
                                           clear_transaction=False)
                    return numpy.frombuffer(data,dtype=numpy_type)
                return msg[len(cmd_name):].strip(b" ").decode()

    @lazy_init
    def get_channel(self, channel_id, mode=None, switch=None, switch_name=None):
        if 0 < channel_id <= 6:
            return self.channel(
                self, channel_id, mode=mode, switch=switch, switch_name=switch_name
            )
        else:
            raise RuntimeError("maestrio doesn't have channel id %d" % channel_id)

    @lazy_init
    def get_channel_by_name(self, channel_name):
        """<channel_name>: Label of the channel.
        """
        channel_name = channel_name.upper()
        channels = self._channels.get(channel_name)
        if channels is None:
            raise RuntimeError(
                "maestrio doesn't have channel (%s) in his config" % channel_name
            )
        return channels[0]  # first match

    @lazy_init
    def get_channel_by_names(self, *channel_names):
        """<channel_names>: Labels of the channels.
        """
        channels = dict()
        for channel_name in channel_names:
            chans = self._channels.get(channel_name.upper())
            if chans is None:
                raise RuntimeError(
                    "maestrio doesn't have channel (%s) in his config" % channel_name
                )
            else:
                for chan in chans:
                    if chan.channel_id not in channels:
                        channels[chan.channel_id] = chan
                        break
                else:
                    raise RuntimeError(
                        "Can't find a free channel for (%s)" % channel_name
                    )
        return list(channels.values())

    @lazy_init
    def get_input(self, channel_id, mode=None, switch=None, switch_name=None):
        if 0 < channel_id <= 20:
            return self.input(
                self, channel_id, mode=mode, switch=switch, switch_name=switch_name
            )
        else:
            raise RuntimeError("maestrio doesn't have input id %d" % channel_id)

    @lazy_init
    def get_ouput(self, channel_id, mode=None, switch=None, switch_name=None):
        if 0 < channel_id <= 8:
            return self.output(
                self, channel_id, mode=mode, switch=switch, switch_name=switch_name
            )
        else:
            raise RuntimeError("maestrio doesn't have output id %d" % channel_id)

    #prgram functions
    @lazy_init
    def get_program_list(self):
        """
        get program loaded on flash memory
        """
        return [x for x in self.putget("?PRGPROG").split('\n') if x]

    @lazy_init
    def upload_program(self,name,src):
        """
        Upload a program into flash memory
        name -- programe name
        src -- programe source
        """
        self.putget(f"#*PRGPROG {name}",data=src)

    @lazy_init
    def erase_program(self,name):
        """
        Erase a program in flash memory
        """
        self.putget(f"#PRGCLEAR {name}")
        
    @lazy_init
    def get_program_state(self,program_name=None,seq_id=None):
        """
        Get the current state of the program
        """
        seq_exp = re.compile('SEQ([0-9]+)')
        
        def extract_prog_state(line):
            params = line.split()
            g = seq_exp.match(params[0])
            if g:
                seq_nb = int(g.group(1))
                prog_state = self.PROG_STATE[params[1]]
                try:
                    prog_name = params[2]
                except IndexError:
                    prog_name = ''
                return seq_nb,prog_state,prog_name
            raise RuntimeError(line)
        
        
        if program_name is None and seq_id is None: # get all status
            return_dict = {}
            status = self.putget("?PRGSTATE")
            for line in status.split('\n'):
                if line:
                    seq_nb,prog_state,prog_name = extract_prog_state(line)
                    return_dict[seq_nb] = prog_state,prog_name
            return return_dict
        
        if program_name is not None:
            line = self.putget(f"?PRGSTATE {program_name}")
            return self.PROG_STATE[line]
        if seq_id is not None:
            seq_id = int(seq_id)
            line = self.putget(f"?PRGSTATE SEQ{seq_id}")
            return extract_prog_state(line)[1:]

    @lazy_init
    def get_program_source(self,program_name,code_type=''):
        """
        get the source of a program name uploaded in the flash memory.
        """
        return self.putget(f"?PRGPROG {program_name} {code_type}")
    
    @lazy_init
    def get_program_var_info(self,program_name):
        """
        get variable information from a program name.
        """
        vars_info = {}
        reply = self.putget(f"?PRGVARINFO {program_name}")
        match_var_info = re.compile(r'^([A-z0-0_]+) +([0-9]+) +(\w+) .*$')
        for line in reply.split('\n'):
            g = match_var_info.match(line)
            if g is None: continue
            vars_info[g.group(1)] = (int(g.group(2)),
                                     numpy.uint32 if g.group(3) == 'UNSIGNED' else numpy.int32)
        return vars_info

    @lazy_init
    def get_program_var_values(self,program_name,*var_names):
        """
        get current values of program variables.
        program_name -- the program name
        *var_names -- can restrict to a list of variables names. if empty all

        return a dictionary with VARIABLE_NAME:VARIABLE_VALUE
        """
        vars_values = {}
        vars_info = self.get_program_var_info(program_name)
        var_names_string = ' '.join(var_names)
        if not var_names:
            var_names = set(vars_info.keys())
        else:
            var_names = set(x.upper() for x in var_names)
            
        var_array_names = var_names.intersection(set(x for x,y in vars_info.items() if y[0] > 1))
        var_scalar_names = var_names - var_array_names
        #get all scalars
        #todo... for now, not possible to do it in one go :-(
        for vname in var_scalar_names:
            vars_values[vname] = int(self.putget(f"?PRGVAR {program_name} {vname}"))
        #Now array
        for vname in var_array_names:
            reply = self.putget(f"?*PRGVAR {program_name} {vname}")
            nb,dtype = vars_info[vname]
            vars_values[vname] = reply.astype(dtype)
        return vars_values
            
    @lazy_init
    def set_program_var_values(self,program_name,**var_values):
        """
        Set var value to the <program_name>
        var_values is a dictionary where key is a name of 
        the var and value is the value of the mac.
        ie: VARTIME=range(100) or NB=10
        """
        var_info = self.get_program_var_info(program_name)
        for vname,value in var_values.items():
            vname = vname.upper()
            nb,dtype = var_info[vname]
            if nb > 1:          # array send in binnary
                data = numpy.array(value,dtype=dtype)
                self.putget(f"#*PRGVAR {program_name} {vname}",data=data)
            else:
                self.putget(f"#PRGVAR {program_name} {vname} {value}")
    
    @lazy_init
    def set_program_var_array_values(self,program_name,
                                     var_name,values,
                                     start_range=0,last_range=-1):
        """
        set var array with a specific target range.
        i.e: change values for range 5 to 10 of VARTIME
             var_name=VARTIME, values=[1,2,3,4,5],start_range=5,last_range=9
        """
        var_info = self.get_program_var_info(program_name)
        nb,dtype = var_info[var_name]
        if nb <= 1:
            raise RuntimeError(f"Variable {var_name} in program {program_name} is not an array")
        
        data = numpy.array(values,dtype=dtype)
        if start_range == 0 and last_range == -1:
            self.putget(f"#*PRGVAR {program_name} {var_name}",data=data)
        else:
            if last_range >= 0:
                self.putget(f"#*PRGVAR {program_name} {var_name}[{start_range}:{last_range}]",data=data)
            else:
                self.putget(f"#*PRGVAR {program_name} {var_name}[{start_range}:]",data=data)

    
    @lazy_init
    def get_program_mac_value(self,program_name,*macro_names):
        """
        get current value of the macros defined in **program_name**
        program_name -- the program name
        *macro_names -- can restrict to a list of macos names. if empty all
        
        return a dictionary with MACRONAME:MACROVALUE
        """
        macs = {}
        mac_names_string = ' '.join(macro_names)
        for line in self.putget(f"?PRGMAC {program_name} {mac_names_string}").split('\n'):
            if not line: continue
            pos = line.find(':=')
            mac_name,value = line[:pos].strip(),line[pos+2:].strip()
            macs[mac_name] = value
        return macs

    @lazy_init
    def set_program_mac_value(self,program_name,**mac_variables):
        """
        Set macro value to the <program_name>
        mac_variables is a dictionary where key is a name of 
        the mac and value is the value of the mac.
        ie: NFRAMES=100
        """
        for name,value in mac_variables.items():
            self.putget(f"#PRGMAC {program_name} {name} := {value}")

    @lazy_init
    def load_program(self,prog_name,seq_id=1):
        """
        Load a program from the flash memory to a sequencer.
        prog_name -- program name (need to be upload first)
        seq_id -- sequencer id (by default load an the first one)
        """
        self.putget(f"#PRGLOAD SEQ{seq_id} {prog_name}")

    @lazy_init
    def unload_program(self,prog_name,seq_id=None):
        """
        Unload a program from a sequencer.
        prog_name -- the program name
        seq_id -- sequencer id (optional)
        """
        if seq_id is not None:
            self.putget(f"#PRGUNLOAD SEQ{seq_id} {prog_name}")
        else:
            self.putget(f"#PRGUNLOAD {prog_name}")

    @lazy_init
    def run_program(self,name,seq_id=None):
        """
        Run a program
        """
        if seq_id is None:
            self.putget(f"#PRGRUN {name}")
        else:
            self.putget(f"#PRGRUN SEQ{seq_id} {name}")

    @lazy_init
    def stop_program(self,name,seq_id=None):
        """
        Stop a program
        """
        if seq_id is None:
            self.putget(f"#PRGSTOP {name}")
        else:
            self.putget(f"#PRGSTOP SEQ{seq_id} {name}")

    @lazy_init
    def get_data(self,nb_counters=None):
        """
        Download all available data.
        nb_counters -- number of counters defined in the STORELIST
        """
        
        # Check that samples are available, otherwise, this generate an error and flood the maestrio logs
        nsamples = int(self.putget('?DAQNSAMPL'))
        if nsamples == 0:
            return None
        
        data = self.putget("?*DAQDATA")
        if nb_counters is not None:
            data.shape = -1,nb_counters
#        data.shape = nsamples, -1
        
        return data

    @autocomplete_property
    @lazy_init
    def counters(self):
        return self._counters_container.counters

    @lazy_init
    def _read_smart_acc_config(self):
        smart_config = self.putget("?SACCCFG")
        exp = re.compile(r'^(CH[1-6]|IN[0-9]+) +(COUNT +(?:UP|UPDOWN)|INTEGR|READ|SAMPLE|TRACK) +'\
                         'SRC +(I[0-9]+|C[1-6])')

        smart_acc_cfg = {}
        for line in smart_config.split('\n'):
            if not line: continue
            
            g = exp.match(line)
            if g is not None:
                channel_name = g.group(1)
                channel_mode_name = g.group(2).replace(' ','_')
                try:
                    channel_mode = SMART_ACC_MODE[channel_mode_name]
                except KeyError:
                    raise KeyError("Can't find smart accumulator type {channel_type_name}")
                smart_acc_cfg[channel_name] = {'mode':channel_mode,
                                               'source':g.group(3)}
            else:
                not_defined = re.compile(r'^(CH[1-6]|IN[0-9]+) +not configured')
                g = not_defined.match(line)
                if g is None:
                    #Try old compatibility to be removed in future.
                    old_exp = re.compile(r'^SACC([0-9]+)')
                    g = old_exp.match(line)
                    if g is not None:
                        channel_id = int(g.group(1))
                        if channel_id <= 20:
                            smart_acc_cfg['IN%d' % channel_id] = {'mode': SMART_ACC_MODE.COUNT_UP,
                                                                  'source': 'I%d' % channel_id}
                        else:
                            channel_id -= 20
                            smart_acc_cfg['CH%d' % channel_id] = {'mode': SMART_ACC_MODE.UNDEF,
                                                                  'source': 'C%d' % channel_id}
                            
                        continue
                    raise RuntimeError(f"Cannot parse smart accumulator config line: {line}")
                
                channel_name = g.group(1)
                smart_acc_cfg[channel_name] =  {'mode':SMART_ACC_MODE.UNDEF,
                                               'source':None}
        return smart_acc_cfg
