import weakref
import gevent
from bliss.controllers.counter import CounterController
from bliss.common.counter import Counter as CounterBase
from bliss.common.counter import IntegratingCounter as IntegratingCounterBase
from bliss.scanning.chain import AcquisitionMaster

class MaestrioDefaultAcquisitionMaster(AcquisitionMaster):
    def __init__(self,controller,maestrio,
                 count_time=None,npoints=1,ctrl_params=None):
        super().__init__(controller,ctrl_params=ctrl_params,
                         prepare_once=False,#True,
                         start_once=False)# True)
        self.count_time = count_time
        self.maestrio = maestrio
        self._sources = {}
        
    def prepare(self):
        self.maestrio.putget("ACQTRIG SOFT")
        self.maestrio.putget("ACQHWTRIG DI1") # if not set can run program
        self._sources = {c:"IN%d" % c.channel\
                         for c in sorted(self._counters,key=lambda c:c.channel)}
        self.maestrio.putget("ACQSRC TIMER %s" % ' '.join(self._sources.values()))
        self.maestrio.putget("ACQLENGTH %fS" % self.count_time)
        self.maestrio.putget("ACQNSAMPL %d" % 1) #self.npoints)

    def start(self):
        self.maestrio.run_program("#ACQ")

    def trigger(self):
        self.trigger_slaves()
        self.maestrio.putget("#PRGEVENT #ACQ")
        #Hack for now
        #Maestrio can only do an equivalent of **runc_ct**
        #to be removed in future
        self.wait_ready()
        data = self.maestrio.get_data(nb_counters=len(self._sources) + 1) # + 1 TIMER is always return
        data_line = data[0]
        self.channels.update({c.name:d for c,d in zip(self._sources,data_line[1:])})

    def trigger_ready(self):
        status = self.maestrio.get_program_state("#ACQ")
        return status != self.maestrio.PROG_STATE
    
    def wait_ready(self):
        while True:
            status = self.maestrio.get_program_state("#ACQ")
            if status != self.maestrio.PROG_STATE.RUN:
                break
            gevent.sleep(20e-3)
            
    def stop(self):
        self.maestrio.stop_program("#ACQ")

    def reading(self):
        nbpoints = self.npoints
        while(npoints):
            status = self.maestrio.get_program_state("#ACQ")
            if status != self.maestrio.PROG_STATE.RUN:
                break
            npoints -= self.try_publish()
            gevent.sleep(0)
        self.try_publish()
        
    def try_publish(self):
        try:
            data = self.maestrio.get_data(nb_counters=len(self._sources))
        except RuntimeError:
            pass
        else:
            self.channels.update({c.name:d for c,d in zip(self._sources,data)})
            return len(data)
        return 0

class MaestrioSamplingCounter(CounterBase):
    def __init__(self,counter_name,controller,channel_name):
        super().__init__(counter_name,controller)
        self._channel = channel_name
    @property
    def channel(self):
        return self._channel

class MaestrioIntegratingCounter(IntegratingCounterBase):
    def __init__(self,counter_name,controller,channel_name):
        super().__init__(counter_name,controller)
        self._channel = channel_name
    @property
    def channel(self):
        return self._channel
        
class MaestrioCounterController(CounterController):
    def __init__(self,maestrio_ctrl,config_tree):
        super().__init__(maestrio_ctrl.name,register_counters=True)
        
        self._maestrio_ctrl = weakref.proxy(maestrio_ctrl)
        SMART_ACC_MODE = self._maestrio_ctrl.SMART_ACC_MODE
        channels_cfg_list =  config_tree.get('channels',[])
        if channels_cfg_list:
            smart_acc_config = self._maestrio_ctrl._read_smart_acc_config()
            for channels_cfg in channels_cfg_list:
                cnt_name = channels_cfg.get('counter_name')
                if cnt_name is not None:
                    channel = channels_cfg['channel']
                    smart_acc_config_channel = smart_acc_config[channel]
                    
                    if smart_acc_config_channel.get('mode') in [SMART_ACC_MODE.COUNT_UP,
                                                                SMART_ACC_MODE.INTEGR]:
                        MaestrioIntegratingCounter(cnt_name,self,channel)
                    else:
                        MaestrioSamplingCounter(cnt_name,self,channel)
                        
    def get_acquisition_object(self,acq_params,ctrl_params,parent_acq_params):
        return MaestrioDefaultAcquisitionMaster(self,self._maestrio_ctrl,**acq_params)
    
    def get_default_chain_parameters(self, scan_params, acq_params):
        return {"count_time": acq_params.get("count_time", scan_params["count_time"])}
